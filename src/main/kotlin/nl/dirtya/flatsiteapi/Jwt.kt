package nl.dirtya.flatsiteapi

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.io.Decoders.BASE64
import io.jsonwebtoken.security.Keys
import java.util.*
import java.util.function.Function

class Jwt
constructor(private val secret: String, private val expiration: Long) {

    private val key = Keys.hmacShaKeyFor(BASE64.decode(secret))

    fun generateToken(subject: String, iat: Long): String {
        val claims: Map<String, Any> = HashMap()
        return createToken(claims, subject, iat)
    }

    private fun createToken(claims: Map<String, Any>, subject: String, iat: Long): String {
        return Jwts.builder()
            .setClaims(claims)
            .setSubject(subject)
            .setIssuedAt(Date(iat))
            .setExpiration(Date(iat + expiration))
            .signWith(key)
            .compact()
    }

    fun validateToken(token: String?, userId: String): Boolean {
        val jws = Jwts.parserBuilder()
            .setSigningKey(key)
            .build()
            .parseClaimsJws(token)
        val claims = jws.body

        return claims.subject == userId && !claims.expiration.before(Date())
    }
}
