package nl.dirtya.flatsiteapi

fun main(args: Array<String>) {
    val secret: String = if (args.isNotEmpty()) {
        args[0]
    } else {
        "VRTQwDdSIAEabipK6YUp+lMNL4nU8j7fCqNHbz8lvGU="
    }
    val expiration: Long = 31536000000
    val jwt = Jwt(secret, expiration)

    val subject = "41"
    //val iat: Long =  1674561340000
    val iat: Long =  1674562740000

    val token = jwt.generateToken(subject, iat)
    println(token)
    println(jwt.validateToken(token, subject))
}
