import argparse
from base64 import b64decode

import jwt

parser = argparse.ArgumentParser('JWT Verifier')
parser.add_argument('--secret', '-s', default="VRTQwDdSIAEabipK6YUp+lMNL4nU8j7fCqNHbz8lvGU=", help='Secret to verify JWT signature with')
parser.add_argument('token', help='JWT token to verify')
args = parser.parse_args()

secret = args.secret
token = args.token

print(jwt.decode(token, b64decode(secret.encode('utf8')), algorithms=["HS256"]))

