# JWT Tester

This repo is a barebones implementation of
[jjwt](https://github.com/jwtk/jjwt) in Kotlin, used to create JWTs.
Since they're signed, they're technically a JWS.

`Jwt.kt` is meant to mirror the code we use in our
[API](https://gitlab.com/dirty-a/api), which might not be open-source
yet. This repo is meant to help in that transition, by ensuring that
our keys are properly and securely signed. This *can* be open-source,
since we use secrets that are never used anywhere else.

`verify.py` is a script that uses
[PyJWT](https://pyjwt.readthedocs.io/en/latest/) to verify the
generated JWS.

## Check that API is broken
1. `git checkout api-mirror`
2. Update `App.kt` to use the same `secret` as production
3. `mvn package -DskipTests`
4. `java -jar target/flatsite-api-0.0.1-SNAPSHOT.jar`
